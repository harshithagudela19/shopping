from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.contrib.auth.forms import UserCreationForm
from .models import Orders, OrderProduct, Product,Register

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True }}

    def create(selfself, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Register
        #serializers=UserCreationForm
        fields='__all__'
        extra_kwargs = {'password': {'write_only': True, 'required': True},'conform_password':{'write_only': True, 'required': True}}

        # def PasswordValidation(selfself, validated_data):
        #     password = Register.objects.password(**validated_data)
        #     conform=Register.objects.conform_password(**validated_data)
        #     if(password == conform):
        #         msg='Password Matched'
        #         return msg
        #     else:
        #         msg='Mismatch'
        #         return msg

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model=Orders
        fields='__all__'

class OrderProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model= OrderProduct
        fields='__all__'
