from django.contrib import admin
from .models import Orders, OrderProduct, Product


# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'image_link', 'price', 'created_on', 'updated_on']


admin.site.register(Product, ProductAdmin)


class OrdersAdmin(admin.ModelAdmin):
    list_display = ['status', 'mode_of_payment', 'total', 'created_order_on', 'updated_order_on']
    exclude = ['user']

admin.site.register(Orders, OrdersAdmin)


class OrderProductAdmin(admin.ModelAdmin):
    list_display = ['order', 'product', 'quantity', 'price']

admin.site.register(OrderProduct, OrderProductAdmin)