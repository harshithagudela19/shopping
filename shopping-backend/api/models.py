from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Register(models.Model):
    username=models.CharField(max_length=20)
    password=models.CharField(max_length=20)
    conform_password=models.CharField(max_length=20,null=True)
    email=models.EmailField()

class Product(models.Model):
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=100)
    image_link = models.URLField()
    price = models.DecimalField(max_digits=8, decimal_places=2)
    created_on = models.DateField(auto_now=True)
    updated_on = models.DateField()

    def __str__(self):
        return f"{self.title} -{self.description} - {self.price} - {self.created_on} - {self.updated_on}"


class Orders(models.Model):
    choice_new = 'New'
    choice_paid = 'Paid'
    choices_of_status = [
        (choice_new, 'New'),
        (choice_paid, 'Paid'),
    ]
    choice_cash = 'Cash On Delivery'
    options_of_pay = [
        (choice_cash, 'Cash On Delivery')
    ]
    status = models.CharField(max_length=4,choices=choices_of_status)
    mode_of_payment = models.CharField(max_length=16,choices=options_of_pay, default=choice_cash)
    total = models.DecimalField(max_digits=8, decimal_places=2)
    created_order_on = models.DateField(auto_now=True)
    updated_order_on = models.DateField()
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.status} - {self.mode_of_payment} - {self.total} - {self.created_order_on} - {self.updated_order_on}"


class OrderProduct(models.Model):
    order = models.ForeignKey(Orders, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return f"{self.order} - {self.product} - {self.quantity} - {self.price}"