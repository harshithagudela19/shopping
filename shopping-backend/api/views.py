from django.shortcuts import render
from rest_framework import viewsets
from django.contrib.auth.models import User
from .models import Register,Product,OrderProduct,Orders
#from rest_framework.decorators import api_view
from django.contrib.auth.forms import UserCreationForm
from .serializers import UserSerializer, RegisterSerializer,ProductSerializer,OrdersSerializer,OrderProductsSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, id):
        return self.list(request, id)

    def post(self, request):
        msg = 'Created'
        return render(self.create(request), {msg: 'msg'})


#@api_view(['POST'])
class RegisterViewSet(viewsets.ModelViewSet):
    queryset = Register.objects.all()
    serializer_class = RegisterSerializer

    def post(self,request):
        return render(self.PasswordValidation(request))

class ProductListViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer

class OrderProductViewSet(viewsets.ModelViewSet):
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductsSerializer