# Generated by Django 3.1.4 on 2021-01-04 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_register_conform_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image_link',
            field=models.URLField(max_length=200000),
        ),
    ]
